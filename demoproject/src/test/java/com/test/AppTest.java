package com.test;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\driver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.facebook.com/");
    	System.out.println("testing maven project");
        driver.quit();
    }
    
    @Test
    public void shouldAnswerWithTrue2()
    {
	   System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\driver\\geckodriver.exe");	
		  
		  WebDriver driver  = new FirefoxDriver();
		  driver.get("https://www.facebook.com/");
    	  System.out.println("testing maven project");
    	  driver.quit();
    }
    @Test
    public void shouldAnswerWithTrue3()
    {
	 System.setProperty("webdriver.ie.driver",  System.getProperty("user.dir")+"\\driver\\IEDriverServer.exe");
		
		WebDriver driver = new InternetExplorerDriver();
		driver.get("https://www.facebook.com/");
		System.out.println("testing maven project");
		driver.close(); 
        driver.quit();
           }

    
    
}
